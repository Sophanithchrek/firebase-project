
const auth = firebase.auth()

function signUp() {

    window.location.href = "signup.html"

}

function requestLogin() {

    var email = document.getElementById('email_field').value
    var pass = document.getElementById('password_field').value

    const promise = auth.signInWithEmailAndPassword(email, pass)
    promise.then(res => {
        let {za} = res.user
        console.log(za," >>> token")
        // alert("sign in " + email)
        // location.replace(`${window.location.origin}/dashboard`)
    })
    promise.catch(e => alert(e.message))

}

function requestLoginWithGoogle() {
    window.sessionStorage.setItem('pending', 1);
    let provider = new firebase.auth.GoogleAuthProvider()
    // firebase.auth().signInWithRedirect(provider)
    firebase.auth().signInWithPopup(provider).then(function (result) {
        let {za} = result.user
        console.log(za," >>> token")
        console.log("Success login with google account")
        alert("OK")
    }).catch(function (err) {
        console.log(err)
        console.log("Error happened")
        alert("OK")
    })
}

auth.onAuthStateChanged(function(user) {

    if (user) {
        // User is success signin...
        var email = user.email
        alert("Active user is: "+email)

    } else {
        alert("No active user")
    }

})
